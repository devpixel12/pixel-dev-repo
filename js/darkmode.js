﻿$(document).ready(function ($) {
    var mode = localStorage.getItem('mode');
    if (mode)
        $('body').addClass(mode);

    $(".darkmode").click(function () {
        $("body").addClass("darkclass");
        localStorage.setItem('mode', 'darkclass');
    });

    $(".normalmode").click(function () {
        $("body").removeClass("darkclass");
        localStorage.setItem('mode', null);
    });
});